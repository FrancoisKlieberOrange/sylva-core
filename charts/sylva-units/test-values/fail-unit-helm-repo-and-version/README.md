This test ensures that we fail on a unit that would have both 'repo' and 'helmrelease_spec.chart.spec.version'.

(see https://gitlab.com/sylva-projects/sylva-core/-/issues/1671)
